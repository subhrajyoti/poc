import express from "express";
import wkhtmltopdf from "wkhtmltopdf";
import {saveToPdf} from "./pdf.helper";
import {createWord} from "./createWord";
import bodyParser from "body-parser";
import { createExcel } from "./createExcel";
wkhtmltopdf.command = "C:\\Users\\vsasjun\\Documents\\example1\\tryy2\\wkhtmltopdf\\bin\\wkhtmltopdf.exe";
var app = express()
app.use(bodyParser());
var ex1;
app.post("/pdf", (req, res) => {
    saveToPdf(req.body, ()=> {
        res.send("Pdf created successfully.");
    });
});

app.post("/word", (req, res) => {
    createWord(req.body, ()=> {
        res.send("Docx file created successfully.");
    })
});

app.post("/excel", (req, res) => {
    createExcel(req.body, ()=> {
        res.send("Excel sheet created successfully.");
    })
});

app.listen("3000");