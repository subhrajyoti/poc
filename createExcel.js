import excel from "excel4node";

const createExcel = (params,callBack) => {
    var workbook = new excel.Workbook();
    var worksheet = workbook.addWorksheet('Sheet 1');
    var style = workbook.createStyle({
        font: {
            color: '#FF0800',
            bold: true,
            size: 20
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -'
    });
    var bodyStyle = workbook.createStyle({
        font: {
            color: '#000000',
            size: 16
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -'
    });
    var options = {
        margins: {
          left: 1.5,
          right: 1.5,
        },
      };
    worksheet.cell(1, 1).string("NAME").style(style);
    worksheet.cell(1, 2).string("DESCRIPTION").style(style);
    let count = 2;
    params.room.map((room)=> {
        worksheet.cell(count, 1).string(room.name, options).style(bodyStyle);
        worksheet.cell(count, 2).string(room.description, options).style(bodyStyle);
        count= count+1;
    })
    workbook.write('Excel.xlsx');
    callBack();
}
export {
    createExcel
}