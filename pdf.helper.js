import wkhtmltopdf from "wkhtmltopdf";
import * as fs from "fs";
import Handlebars from "handlebars";
const saveToPdf = ( params, callback) => {
    fs.readFile('./index.html',{encoding: "utf-8"}, (err,file)=> {
        var template = Handlebars.compile(file);
        var html = template(params);
        wkhtmltopdf(html, {
            output: 'demos.pdf',
        },callback);
    })
}
export {
    saveToPdf
}